1. What is ORM?
=>(Object Relational Mapper) ORMs provide a high-level abstraction upon a relational database that allows a developer to write Python code instead of SQL to create, read, update and delete data and schemas in their database. Developers can use the programming language they are comfortable with to work with a database instead of writing SQL statements or stored procedures.

Ref-https://www.fullstackpython.com/object-relational-mappers-orms.html

==> To view the query
import logging
l = logging.getLogger('django.db.backends')
l.setLevel(logging.DEBUG)
l.addHandler(logging.StreamHandler())

======================================================================================================================================

2. How to filter data?
Question.objects.all()
Question.objects.filter(parameter)
Question.objects.filter(title = 'cars2', content ='ferrari') 
Question.objects.order_by('id')
Question.objects.order_by('title')
Resources-https://tutorial.djangogirls.org/en/django_orm/
          https://docs.djangoproject.com/en/2.2/topics/db/queries/


======================================================================================================================================


3. How to join data from multiple tables?
Answer.objects.select_related('question') 
Answer.objects.filter(question__title='Vote')

=======================================================================================================================================

4. What is the n + 1 query problem? How to avoid it in Django?

>>> 



=======================================================================================================================================


5. How to calculate sum, average etc?

from django.db.models import Avg
>>> Book.objects.all().aggregate(Avg('price'))
>>> Book.objects.aggregate(average_price=Avg('price'))

from django.db.models import Sum
>>> Book.objects.all().aggregate(Sum('price'))
>>> Book.objects.aggregate(average_price=Sum('price'))


=======================================================================================================================================


6. What is a queryset. How is it different from a list?
>>>A QuerySet represents a collection of objects from your database. It can have zero, one or many filters. Filters narrow down the query results based on the given parameters. In SQL terms, a QuerySet equates to a SELECT statement, and a filter is a limiting clause such as WHERE or LIMIT.
>>>Book_list = list(Book.objects.all()) 

=======================================================================================================================================


7. What is a Model Manager? How to create a custom model manager. When are they used?
>>> A Manager is the interface through which database query operations are provided to Django models. At least one Manager exists for every model in a Django application.s


from django.db import models

class PollManager(models.Manager):
    def with_counts(self):
        from django.db import connection
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT p.id, p.question, p.poll_date, COUNT(*)
                FROM polls_opinionpoll p, polls_response r
                WHERE p.id = r.poll_id
                GROUP BY p.id, p.question, p.poll_date
                ORDER BY p.poll_date DESC""")
            result_list = []
            for row in cursor.fetchall():
                p = self.model(id=row[0], question=row[1], poll_date=row[2])
                p.num_responses = row[3]
                result_list.append(p)
        return result_list

class OpinionPoll(models.Model):
    question = models.CharField(max_length=200)
    poll_date = models.DateField()
    objects = PollManager()

class Response(models.Model):
    poll = models.ForeignKey(OpinionPoll, on_delete=models.CASCADE)
    person_name = models.CharField(max_length=50)
    response = models.TextField()

=======================================================================================================================================




Best Resources==>https://books.agiliq.com/projects/django-orm-cookbook/en/latest/introduction.html